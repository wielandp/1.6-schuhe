package io.fp.shoes;

public class BoxException extends Exception {

	public BoxException() {
		super("Something went wrong.");
	}

	public BoxException(String arg0) {
		super(arg0);
	}

	public BoxException(Throwable cause) {
		super(cause);
	}

	public BoxException(String message, Throwable cause) {
		super(message, cause);
	}

	public BoxException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
