package io.fp.shoes;

import java.util.ArrayList;
import java.util.List;

public class Shelf {
	
	private ArrayList<ArrayList<ShoeBox<? extends Shoes>>> shelf;
	private int height;
	private int width;
	
	public Shelf(int height, int width) {
		this.height = height;
		this.width = width;
		shelf = new ArrayList<>();
		for(int i = 0; i < height; i++) {
			shelf.add(new ArrayList<ShoeBox<? extends Shoes>>());
			for(int j = 0; j < width; j++) {
				shelf.get(i).add(null); //"Leere" Plaetze 
			}			
		}
	}
	
	
	public void addBox(int x, int y, ShoeBox<? extends Shoes> box) throws BoxException {
		if(x<0 || y<0|| x >= this.height || y >= this.width) throw new IndexOutOfBoundsException("There is no such place in the shelf!");
		if(shelf.get(x).get(y) != null) throw new BoxException("There is already a different box in " + x +"/" + y + "!");
		shelf.get(x).set(y, box);
	}
	
	public void removeBox(int x, int y) throws BoxException {
		if(x<0 || y<0|| x >= this.height || y >= this.width) throw new IndexOutOfBoundsException("There is no such place in the shelf!");
		if(shelf.get(x).get(y) == null) throw new BoxException("No item to remove in " + x +"/" + y + "!");
		shelf.get(x).set(y, null);
	}
	
	public ShoeBox<? extends Shoes> get(int x, int y) {
		return shelf.get(x).get(y);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(List<ShoeBox<? extends Shoes>> shelfY : shelf) {
			for(ShoeBox<? extends Shoes> box : shelfY) {		
				sb.append("|" + (box==null?"----free----":box.toString()) + "|");
			}
			sb.append('\n');

		}
		return sb.toString();
	}
	
}
