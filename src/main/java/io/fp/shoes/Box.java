package io.fp.shoes;

public abstract class Box <T>{
	private T content;
	
	public Box() {
		super();
	}
	
	public Box(T content) {
		this.content = content;
	}
	
	public void putObjectInBox(T obj) throws BoxException {
		if(content == null) {
			content=obj;
		}
		else {
			throw new BoxException("Box is full!");
		}
	}
	
	public T removeObjectInBox() {
		T temp = content;
		content=null;
		return temp;
	}
	
	public T getObjectInBox() {
		return content;
	}

	@Override
	public String toString() {
		return "[" + content + "]";
	}
	
}
