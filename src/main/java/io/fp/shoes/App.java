package io.fp.shoes;

public class App {

	public static void main(String[] args) throws BoxException {
		Sneakers sneakers = new Sneakers("AirMax", 42.5);
		Sneakers sneakers2 = new Sneakers("AirForce1", 44.0);
		Boots boots = new Boots("Stiefel braun", 39.0);
		Boots boots2 = new Boots("Stiefel schwarz", 37.5);

		ShoeBox<Boots> sb1 = new ShoeBox<>();
		ShoeBox<Sneakers> sb2 = new ShoeBox<>();
		ShoeBox<Boots> sb3 = new ShoeBox<>();
		ShoeBox<Sneakers> sb4 = new ShoeBox<>();
		
		sb1.putObjectInBox(boots);
		sb2.putObjectInBox(sneakers);
		sb3.putObjectInBox(boots2);
		sb4.putObjectInBox(sneakers2);
		
		
		System.out.println(sb1);
		System.out.println(sb2);
		System.out.println("\n_____________________________________________\n");
		
		
		/*
		 * Aufgabenteil d)
		 */
		
		System.out.println("Create shelf with x=3 y=5");
		Shelf shelf = new Shelf(3,5);
		
		System.out.println("Leeres Regal:\n"+shelf);
		
		//Adding boxes to shelf
		System.out.println("Add sb1 to 0/4");
		shelf.addBox(0,4,sb1);
		System.out.println("Add sb2 to 2/2");
		shelf.addBox(2,2,sb2);
		System.out.println("Add sb3 to 0/1");
		shelf.addBox(0,1,sb3);
		System.out.println("Add sb4 to 2/1");
		shelf.addBox(2,1,sb4);
		
		
		System.out.println("\nContents of shelf:");
		System.out.println(shelf);


		//shelf.addBox(42, 32, sb1);  //IndexOutOfBoundsException
		//System.out.println("Add sb2 to 2/2 to throw an Exception");
		//shelf.addBox(2,2,sb2); //Exception

		
		//Removing boxes from shelf
		System.out.println("Remove Box from 2/2");
		shelf.removeBox(2, 2);

		//shelf.removeBox(3, 5); //IndexOutOfBoundsException
		//System.out.println("Remove Box from 2/2 to throw an Exception");
		//shelf.removeBox(2, 2);  //Exception
		
		System.out.println("\nContents of shelf after removing:");
		System.out.println(shelf);
	}

}
