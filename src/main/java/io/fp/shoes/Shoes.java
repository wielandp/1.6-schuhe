package io.fp.shoes;

public abstract class Shoes {
	private String name;
	private double size;	

	public Shoes (String name, double size) {
		this.name = name;
		this.size = size;
	}

	@Override
	public String toString() {
		return "{" + name + ", " + size + "}";
	}

}
