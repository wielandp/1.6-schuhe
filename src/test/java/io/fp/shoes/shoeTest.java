package io.fp.shoes;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * shoeTest
 */
public class shoeTest {

    ShoeBox<Boots> box;

    @BeforeEach
    void setUp(){

        box = new ShoeBox<Boots>(new Boots("boot", 42));

    }

    @Test
    void boxTest(){

        assertThrows(BoxException.class, ()->{

            box.putObjectInBox(new Boots("antiboot", 42));
        });
    }
}